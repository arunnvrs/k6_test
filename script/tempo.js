import http from 'k6/http';
import { check, group, sleep, fail } from 'k6';
import papaparse from 'https://jslib.k6.io/papaparse/5.1.1/index.js';
import { SharedArray } from "k6/data";
import exec  from 'k6/execution';
import { Trend, Rate, Counter, Gauge } from 'k6/metrics';
import sql from 'k6/x/sql';

export const TrendRTT = new Trend('RTT');
export const RateContentOK = new Rate('Content OK');
export const GaugeContentSize = new Gauge('ContentSize');
export const CounterErrors = new Counter('Errors');
export const options = {
	
  scenarios: {
    checkendpoint: {
      executor: 'constant-vus',
      exec: 'checkendpoint',
	  env: { MY_TEST_ID: __ENV.testid },
      vus: __ENV.testvu,
      duration: __ENV.testduration,
    },
    anothercheckendpoint: {
      executor: 'per-vu-iterations',
      exec: 'checkendpoint',
	  env: { MY_TEST_ID: '2' },
      vus: 50,
      iterations: 100,
      startTime: '10s',
      maxDuration: '60s',
    },
  },
  thresholds: {
    'RTT': ['p(99)<300', 'p(70)<250', 'avg<200', 'med<150', 'min<100'],
    'Content OK': ['rate>0.95'],
    'ContentSize': ['value<4000'],
    'Errors': ['count<100'],
	'checks{myTag:errorrate}': ['rate>0.95'],
  },
 
};
const db = sql.open('postgres', 'postgres://postgres:JAD@password@35.200.237.125:5432/postgres?sslmode=disable');
const csvData = new SharedArray("another data name", function() {
    return papaparse.parse(open('./Book.csv'), { header: true }).data;
});

function getEndPointUrl(){
 
    return csvData[0].endpoint;
}
export function checkendpoint() {
 console.log(`step1: scenario ran for ${new Date() - new Date(exec.scenario.startTime)}ms`);
let my_id = getEndPointUrl()
  const url = `${my_id}`;

  let res = http.get(url);
  const contentOK = res.json('name') === '1';
  check(res, {
    'status is 200': (r) => r.status === 200
  },{myTag:'errorrate'});
  TrendRTT.add(res.timings.duration);
  RateContentOK.add(contentOK);
  GaugeContentSize.add(res.body.length);
  CounterErrors.add(!contentOK);
 const scenarioname=exec.scenario.name;
sleep(1);
  
}
export function setup() {
  db.exec(`CREATE TABLE IF NOT EXISTS executionsummary ( DateTime TIMESTAMPTZ, Testid INT, Testname VARCHAR(20), Region VARCHAR(20), Testduration VARCHAR(20), VU INT, Teststatus VARCHAR(20))`);
}

export function teardown() {
const testid = __ENV.testid;
  let testname = 'scenarioname';
  let region = __ENV.testregion;
  const testduration = 50;
  const vus = 20;
  db.exec("INSERT INTO executionsummary (DateTime, Testid, Testname, Region, Testduration, VU, Teststatus) VALUES (current_timestamp,$1,$2,$3,$4,$5,'pass')",[testid,testname,region,testduration,vus]);
  db.close();
}

