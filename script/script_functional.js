import { describe } from 'https://jslib.k6.io/expect/0.0.5/index.js';
import { Httpx } from 'https://jslib.k6.io/httpx/0.0.4/index.js';
export const options = {
  thresholds: {
    checks: [{ threshold: 'rate == 1.00', abortOnFail: true }],
  },
  vus: 1,
  iterations: 1,
};
const session = new Httpx({ baseURL: 'http://34.125.43.222' });
export default function testSuite() {
  describe('Fetch a list of public crocodiles', (t) => {
    const response = session.get('/api/v1/products');
    t.expect(response.status)
      .as('response status')
      .toEqual(200)
      .and(response)
      .toHaveValidJson()
      .and(response.json().length)
      .as('number of crocs')
      .toBeGreaterThanOrEqual(1)
  });
}

